<?php

class MigrateController extends CI_Controller
{
    public function run()
    {
        $this->load->model('migration');
        echo "Migration runing!" . PHP_EOL;
        echo "-------------------------------" . PHP_EOL . PHP_EOL . PHP_EOL;
        $states = $this->migration->execute();
        foreach ($states as $state) {
            echo $state['query'] . PHP_EOL;
            echo "Is success: " . $state['isSuccess'] . PHP_EOL;
            echo "-------------------------------" . PHP_EOL;
        }
    }
}