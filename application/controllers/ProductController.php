<?php

/**
 * Created by PhpStorm.
 * User: marek
 * Date: 08.07.2017
 * Time: 18:32
 */
class ProductController extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *   $this->load()
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */


    public function index()
    {
        $this->load->model('product');
        $this->load->model('ProductCategory');
        $this->load->library('session');

        $productId = $this->input->post('productId');
        $count = $this->input->post('count');

        $data = [];

        $eshop = $this->session->userdata('eshop');

        if (!empty($eshop)) {
            $data = json_decode($eshop, true);
        }
        if (is_numeric($productId) && is_numeric($count)) {
            if (isset($data[$productId]) && is_numeric($data[$productId])) {
                $count += $data[$productId];
            }
            $data[$productId] = (int)$count;
            $this->session->set_userdata('eshop', json_encode($data));
        }

        $countProduct = 0;
        foreach ($data as $product) {
            $countProduct += $product;
        }
        $products = Product::all(['id', 'name', 'description']);

        $this->load->view('eshop/products', ["products" => $products, 'countProduct' => $countProduct]);
    }

    public function detail($id)
    {
        $this->load->model('product');
        $this->load->model('ProductCategory');

        $this->load->library('session');
        $product = Product::find($id);
        $data = [];

        $eshop = $this->session->userdata('eshop');

        if (!empty($eshop)) {
            $data = json_decode($eshop, true);
        }
        $countProduct = 0;
        foreach ($data as $product) {
            $countProduct += $product;
        }
        $this->load->view('eshop/product', ["product" => $product, 'countProduct' => $countProduct]);
    }

    public function order()
    {
        $this->load->library('session');
        $this->load->model('product');
        $this->load->model('order');

        $this->load->model('ProductCategory');
        $eshop = $this->session->userdata('eshop');
        $data = json_decode($eshop, true);

        $result = [];
        if (!empty($eshop)) {
            $booking = $this->input->post('booking');

            if ($booking) {
                $order = new Order();
                $order->name = $this->input->post('name');
                $order->address = $this->input->post('address');
                $order->save();

                foreach ($data as $productId => $count) {
                    $product = Product::find($productId);
                    if (!empty($product)) {
                        $order->products()->attach($productId, ['count' => $count, 'price_one' => $product->price]);
                        $order->save();
                    }
                }
                $this->session->set_userdata('eshop', null);
                $data = [];
                $this->load->helper('url');
                redirect('/products');
            }


        }
        foreach ((array)$data as $productId => $count) {
            $result[] = ['product' => Product::find($productId), 'count' => $count];

        }
        $this->load->view('eshop/order', ["products" => $result]);

    }
}
