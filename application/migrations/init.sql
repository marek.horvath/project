CREATE TABLE IF NOT EXISTS `app_users` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL,
    `surname` varchar(30) NOT NULL,
	`email` varchar(30) NOT NULL UNIQUE,
    `password` varchar(30) NOT NULL,
    PRIMARY KEY (ID)
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `app_orders` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL,
    `address` varchar(30) NOT NULL,
    `user_id` int unsigned,
	 `price` double NOT NULL,
    PRIMARY KEY (ID),
     FOREIGN KEY (user_id) REFERENCES app_users(id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `app_products` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL,
    `image` varchar(30) NOT NULL,
    `price` double NOT NULL,
    `description` varchar(30) NOT NULL,
    PRIMARY KEY (ID)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `app_orders_products` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
	`product_id` int unsigned NOT NULL,
	`order_id` int unsigned NOT NULL,
    `count` int NOT NULL,
    `price_one` double NOT NULL,
    PRIMARY KEY (ID),
     FOREIGN KEY (product_id) REFERENCES app_products(id),
     FOREIGN KEY (order_id) REFERENCES app_orders(id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `app_product_categories` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(30) NOT NULL,
    PRIMARY KEY (ID)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `app_products_categories` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
	`product_id` int unsigned NOT NULL,
	`category_id` int unsigned NOT NULL,
    PRIMARY KEY (ID),
     FOREIGN KEY (category_id) REFERENCES app_product_categories(id),
     FOREIGN KEY (product_id) REFERENCES app_products(id)
) ENGINE=InnoDB;