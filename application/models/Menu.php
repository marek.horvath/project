<?php

class Menu extends CI_Model
{
    public function navigation()
    {
        $this->db->select('name, url');
        return $this->db->get('menus')->result();
    }
}