<?php

class Migration extends CI_Model
{
    public function execute()
    {
        $str = file_get_contents(__DIR__ . '/../migrations/init.sql');
        $queries = explode(";", $str);
        $response = [];
        $this->db->trans_start();
        foreach ($queries as $query) {
            if(empty($query))
                continue;
            $response[] = [
                'query' => $query,
                'isSuccess' => $this->db->query($query)
            ];
        }
        $this->db->trans_complete();
        return $response;
    }
}