<?php

/**
 * Created by PhpStorm.
 * User: marek
 * Date: 08.07.2017
 * Time: 15:50
 */
use Illuminate\Database\Eloquent\Model;
class Order extends Model
{
    public $table='orders';
    public $timestamps=false;

    public function user()
    {
        return $this->belongsTo('Order','user_id','id');
    }

    public function products()
    {
        return $this->belongsToMany('Product', 'orders_products')->withPivot('count', 'price_one');
    }
}