<?php

/**
 * Created by PhpStorm.
 * User: marek
 * Date: 08.07.2017
 * Time: 16:05
 */
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';
    public $timestamps = false;

    public function orders()
    {
        return $this->belongsToMany('Order', 'orders_products')->withPivot('count', 'price_one');
    }

    public function categories()
    {
        return $this->belongsToMany('ProductCategory', 'products_categories','product_id','category_id');
    }
}