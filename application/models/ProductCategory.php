<?php

/**
 * Created by PhpStorm.
 * User: marek
 * Date: 09.07.2017
 * Time: 12:47
 */
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public $table = 'product_categories';
    public $timestamps = false;

    public function products()
    {
        return $this->belongsToMany('Product', 'products_categories', 'category_id','product_id');
    }
}



