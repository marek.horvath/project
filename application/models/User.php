<?php

/**
 * Created by PhpStorm.
 * User: marek
 * Date: 08.07.2017
 * Time: 15:19
 */
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $table='users';
    public $timestamps=false;

    public function orders()
    {
        return $this->hasMany('Order','user_id','id');
    }
}