<?
require __DIR__ . "/../layouts/header.php";
?>

    <div class="full">
        <div class="wrapper">
            <table class="table table-bordered table-striped table-hover">
                <tr>
                    <th>Name product</th>
                    <th>Number of pieces</th>
                </tr>
              <?
                  foreach ((array) $products as $product) { ?>
                    <tr>
                            <td> <?= $product['product']->name ?></td>
                            <td> <?= $product['count'] ?></td>
                  </tr>
                <? }?>


            </table>
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Name:</label>
                    <div class="col-sm-4">
                        <input type="name" class="form-control" id="name" placeholder="Enter Name" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="address">Address:</label>
                    <div class="col-sm-4">
                        <input type="address" class="form-control" id="address" placeholder="Enter Address" name="address">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-5">
                            <input type="submit" class="btn btn-primary" name="booking" value="Book">
                    </div>
                </div>
            </form>

        </div>
    </div>
<?
require __DIR__ . "/../layouts/footer.php";
?>