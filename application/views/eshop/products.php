<?
require __DIR__ . "/../layouts/header.php";
?>

    <div class="full">
        <a href="order">Card (<?=$countProduct?>)</a>
        <div class="wrapper row">
            <? foreach ($products as $product) { ?>
                <div class="col-sm-4 ">
                    <div class="product">
                        <?= $product->url ?><?= $product->name ?>
                        <div class="description">
                            Description
                        </div>
                        <div class="categories">
                            <strong>Categories: </strong>
                            <? foreach ($product->categories as $category) { ?>
                                <?= $category->name ?>,
                            <? } ?>
                        </div>
                        <div class="row product-action-bar">
                            <form class="pro" method="post">
                                <div class="col-sm-2  ">
                                    <input type="number" name="count" class="num" min="1" max="150" value="1">
                                    <input type="hidden" name="productId"  value="<?=$product->id?>">
                                </div>
                                <div class="col-sm-6">
                                    <input type="submit" class="btn btn-primary" value="Kupit">
                                </div>

                            </form>
                            <div class="col-sm-6 ">
                                <a href="product/<?= $product->id ?>" class="btn  btn-info info" role="button">Viac</a>
                            </div>
                        </div>
                    </div>

                </div>
            <? } ?>
        </div>
    </div>
<?
require __DIR__ . "/../layouts/footer.php";
?>