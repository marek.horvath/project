<!DOCTYPE html>
<html>
<head>
    <title>cms</title>

    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>static/css/index.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>static/css/mobile.css"/>

    <script src="<?= base_url(); ?>static/js/library.js" type="text/javascript"></script>


</head>
<body>
<header>

    <div class="full top-strip">
        <div class="wrapper">
            <div class="col-sm-12">
                <a href="#" class="transition">0915515514</a>
                <a href="#" class="transition">infinium@infinium.sk</a>
            </div>
        </div>
    </div>

    <div class="full box">
        <div class="wrapper row container">
            <div class="col-sm-2 logo"><a href="#"> <img src="<?= base_url(); ?>static/images/logo.png"/></a></div>
            <div class="col-sm-10 menu ">

                <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
                    <div class="">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav  pull-right">
                                <?foreach ($menus as $menu){?>
                                    <li><a href="<?=$menu->url?>"><?=$menu->name?></a></li>
                                <?}?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>

    <div class="poz">

        <img src="<?= base_url(); ?>static/images/poz.jpg"/>

        <div class="text">

            <p><b> <br> Welcome</b></br>

                <b> to INFINIUM advisory services </b>

            </p>

        </div>

    </div>
    <div class="full triangle">
        <img src="<?= base_url(); ?>static/images/triangle_1.png"/>
    </div>


</header>


<main>




    <div class="full img posuv">



        <div class="wrapper">
            <div class="link">
                <a href="#"class="transition">Advisors </a>
            </div>
            <div class="link"> We are proud to offer you our professional skills and knowledge</div>
        </div>
        <div class="wrapper">
            <div class="persons">
                <div class="col-sm-3 item">
                    <div class="foto transition">
                        <a href="#"><img src="<?= base_url(); ?>static/images/foto.jpg"/></a>
                    </div>
                    <div class="name">
                        <br> Meno</br>
                    </div>
                    <div class="info">
                        <br><a href="#"> email</a></br>
                        phone
                    </div>
                </div>
                <div class="col-sm-3 item">
                    <div class="foto transition">
                        <a href="#"><img src="<?= base_url(); ?>static/images/foto.jpg"/></a>
                    </div>
                    <div class="name">
                        <br> Meno</br>
                    </div>
                    <div class="info">
                        <br><a href="#"> email</a></br>
                        phone
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
<div class="full triangle_white">
</div>




<div class="full ">
    <div class="wrapper">
        <div class="row link">
            <a href="#" class="transition">Advisory services </a>
        </div>
        <div class="row" >
            <div class="col-sm-4 button">
                <a href="#" class="hover-button transition light-gray btn btn-default">solar <strong>energy</strong></a>
            </div>
            <div class="col-sm-4 button">
                <a href="#" class="hover-button transition darker-gray btn btn-default">environment</a>
            </div>
            <div class="col-sm-4  button">
                <a href="#" class="hover-button transition dark-gray btn btn-default">development</a>
            </div>
        </div>

    </div>


</div>


<div class="full background">
    <img src="<?= base_url(); ?>static/images/advisor.png"/>
</div>

<div class="ful">
    <div class="wrapper">
        <div class="row text">
            <strong>Partners </strong>
        </div>

        <div class="col-sm-10 menu ">
            <ul >
                <?foreach ($menus as $menu){?>
                    <li><a class="transition" href="<?=$menu->url?>"><?=$menu->name?></a></li>
                <?}?>
            </ul>

        </div>
    </div>
    </div>
</div>
</main>


<footer>
<div class="full background">
    <div class="wrapper set">
        <div class="logo">
            <a href="#"> <img src="<?= base_url(); ?>static/images/logo.png"/></a>
        </div>
        <div class="row text-center">
            Contact
        </div>
        <div class="info">
            <a href="#">ncbvncncn</a>
            <br> <a href="#">34535432131</a></br>
        </div>
        <hr>
        <div class="end">
            <div class="col-lg-4 end1">
                © 2016<a href="#" class="transition ">infinium.sk </a>
            </div>
            <div class="col-lg-4 end1">
                <a href="#" class="transition ">Disclaimer</a>
            </div>
            <div class="col-lg-4 end1"> asdasasd <a href="#" class="transition ">sadsaas</a>
            </div>

        </div>

        </div>
    </div>
</div>


</footer>

</body>
</html>