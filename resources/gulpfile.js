var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');

var jsLibrary = [
    "bower_components/angular/angular.min.js",
    "bower_components/jquery/dist/jquery.min.js",
    "bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js"
];

gulp.task('js', function () {
    return gulp.src(jsLibrary)
        .pipe(concat('library.js'))
        .pipe(gulp.dest('../static/js'));
});

gulp.task('styles', function() {
    gulp.src('sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('../static/css/'))
});

//Watch task
gulp.task('default',function() {
    gulp.watch('sass/**/*.scss',['styles']);
});